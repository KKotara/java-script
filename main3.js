var Account = function (balance, currency){
    this.balance = balance;
    this.currency = currency;
};

var person = (function (){
    var details = {
        firstName: 'Krzysztof',
        lastName: 'Kotara',
    },
    calculateBalance = function(){
        var sum=0;
        for( i =0; i < person.accountslist.length; i++) {
            sum += person.accountslist[i].balance;
        }
        return sum;
    };
    
    return {
        accountslist: [{
            balance: 5,
            currency: 'CHF'
        }],
        addAcount: function(account){
            person.accountslist.push(account);
        },
        sayHello: function(){
            return details.firstName + ' ' + details.lastName + ' ' + calculateBalance();    
        }
    };
})();
var persona = person;
console.log(persona.sayHello());
person.addAcount({
    balance: 15,
    currency: 'PLN'
});
console.log(persona.sayHello());
person.accountslist[1] = new Account(999,'USS');
console.log(persona.accountslist[1].balance +' ' + persona.accountslist[1].currency);
console.log(persona.sayHello());
