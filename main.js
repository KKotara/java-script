var personFactory = function (){
    var details = {
        firstName: 'Krzysztof',
        lastName: 'Kotara',
        accountslist: [{
            balance: 5,
            currency: 'CHF'
        }] 
    };
    return {
        sayHello: function(){
            return details.firstName + ' ' + details.lastName + ' ' + details.accountslist[0].balance + ' ' + details.accountslist[0].currency;    
        }
    };
};
var person = personFactory();
console.log(person.sayHello());
