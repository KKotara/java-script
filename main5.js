class Account { 
    constructor (balance, currency, number){
        this.balance = balance;
        this.currency = currency;
        this.number = number;
    }
};

class Person {
    constructor (firstName, lastName, accountslist){
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountslist = accountslist;
    }
    calculateBalance(){
        var sum=0;
        for( let stan of this.accountslist) {
            sum += stan.balance;
        }
        return sum;
    }
    addAcount(account){
        this.accountslist.push(account);
    }
    filterPositiveAccounts(){
        return this.accountslist.filter(function(konto){
            return konto.balance > 0;
        });
    }
    findAccount(accountNumber){
        return this.accountslist.find(function(number){
            if(number.number === accountNumber){
                return number.number;
            }
        });
    }
    withdraw(accountNumber, amount){
        return  new Promise((resolve, reject) =>{
            let numerKonta = this.findAccount(accountNumber);
            if (numerKonta != undefined && amount <= numerKonta.balance){
                numerKonta.balance -= amount;
                setTimeout(function () {
                  return  resolve(accountNumber + ' ' + numerKonta.balance + ' ' + amount);
                }
                ,3000);
            }
            else{
                return reject ('Brak takiego konta lub błąd salda!');  
                };
        })

    }
    sayHello(){
        return this.firstName + ' ' + this.lastName + ' ' + this.calculateBalance();    
    }
};

const ja = new Person ('Krzysztof','Kotara',[new Account(9999,'JPN',1)]);
ja.addAcount(new Account(9999,'JPN',1));
ja.addAcount(new Account(-1,'JPN',4));
ja.addAcount(new Account(10,'EUR',2));
ja.addAcount(new Account(30,'JPN',4));

ja.withdraw(1,100)
    .then(function(successMessage){
    console.log(successMessage);
    })  
    .catch(function(failMessage){
    console.log(failMessage)
    });
ja.withdraw(3,20)
    .then(function(successMessage){
    console.log(successMessage);
    })
    .catch(function(failMessage){
    console.log(failMessage);
    });